const express = require("express");
const app = express();
const slugify = require("slugify");

const titulo = 'Y18920';

const productos = [{id: 1, nombre:"Coca Cola 500ml", precio: 10.10},
                   {id: 2, nombre:"Pepsi 500ml", precio: 10.05},
                   {id: 3, nombre:"Fanta250ml", precio: 10.07}];

productos.forEach(producto => {
producto.slug = slugify(producto.nombre);
});

app.set("view engine","ejs");
app.set("views","views");
app.use(express.json());
app.use(express.urlencoded({ extended: true })); 


//peticion get
app.get("/productos", (req, res) => {
//res.send("Hola mundo")
res.render("productos", {title:titulo, productos:productos})
});

app.get("/productos_det/:slug", (req, res) => {
  const slug = req.params.slug;
  const productoEncontrado = productos.find(producto => producto.slug === slug);
  res.render("productos_det", { producto: productoEncontrado });
});


app.put("/productos/:slug", (req, res) => {
  const slug = req.params.slug;
  const productoEncontrado = productos.find(producto => producto.slug === slug);

  if (productoEncontrado) {
      productoEncontrado.nombre = req.body.nombre;
      productoEncontrado.precio = parseFloat(req.body.precio);

      res.status(200).json({mensaje: "Producto actualizado"});
    }else{
        res.status(204).json({mensaje: "Producto no encontrado"});
    }
});

app.delete("/productos/:slug", (req, res) => {
  const slug = req.params.slug;
  const productoIndex = productos.findIndex(producto => producto.slug === slug);

  if (productoIndex !== -1) {
      productos.splice(productoIndex, 1);
      res.status(200).json({ mensaje: "Producto eliminado" });
  } else {
      res.status(404).json({ mensaje: "Producto no encontrado" });
  }
});

app.get("/productos/crear_producto", (req, res) => {
  res.render("crear_producto");
});

app.post("/productos", (req, res) => {
  const nuevoProducto = {
      
      id: (Math.max(...productos.map((producto)=> producto.id)) + 1),

      nombre: req.body.nombre,
      precio: parseFloat(req.body.precio),
      slug: slugify(req.body.nombre)
  };

  productos.push(nuevoProducto);

  res.status(201).json({ mensaje: "Producto creado exitosamente" });
});
  

// Inicia el servidor nuevo comentario
app.listen(3000, () => {
  console.log("Servidor puerto 3000");
});

