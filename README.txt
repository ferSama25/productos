----------------------------------------Tabla de Productos---------------------------------------
*Instrucciones de uso*
- Al iniciar el proyecto apareceran todos los productos preestablecidos en el archivo index
- Si se realiza el click sobre estos productos, se podra editar el nombre y el precio del mismo, el boton de guardar los cambios
se habilita luego de realizar una modificacion en alguno de los dos campos.
- En el mismo menu de edición se podra eliminar el producto del listado con el boton de "Eliminar Producto"
- En el menu de inicio se podra dar click al boton de Agregar Producto, este boton nos llevará a cargar los campos de nombre y precio
de los nuevos productos que deseamos añadir a nuestra lista de productos.

